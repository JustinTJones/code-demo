
var wAPIkey = "2456a816428a825b2b76b69a3ff834f0";
var cAPIurl = "http://134.122.11.119";

var Login = {
    view: function (vnode, attrs) {
        return m("form#login", {action: null}, [
            m("h3", "Please log in to see the current weather"),
            m("#notice"),
            m("input#user", {type: "text", placeholder: "Username"}),
            m("input#pass", {type: "password", placeholder: "Password"}),
            m("button#login", {
                type: "submit",
                onclick: function (e) {
                    e.preventDefault();
                    var userStr = document.getElementById("user").value;
                    var passStr = document.getElementById("pass").value;
                    login(userStr, passStr, attrs);
                }
            }, "Login")
        ]);
    }
};

var Weather = {
    data: {},
    view: function (vnode, attrs) {
        if (localStorage.getItem('token').length != 64) {
            console.log("Must be authenticated to do that");
            m.route.set("/login");
        }
        if (!Weather.data.hasOwnProperty("current")) {return m("div.main", m("h2", "Weather not available for Saint Paul"));}
        var tempC = Weather.data.current.temperature;
        var tempF = Math.round(tempC * 1.8 + 32);
        var F = "&deg; F";
        var C = "&deg; C";
        // console.log(tempF);
        return m("div.main", [
            m("h2", "Weather for Saint Paul"),
            m("div.weather", [
                "The current temperature is ",
                m("span#temp", tempF),
                m("span#unit", m.trust(F))
            ]),
            m("button", {onclick: function (e) {
                e.preventDefault();
                var tempSpan = document.getElementById("temp");
                var unitSpan = document.getElementById("unit");
                if (unitSpan.innerHTML.includes('C')) {
                    tempSpan.innerHTML = tempF;
                    unitSpan.innerHTML = F;
                }
                else if (unitSpan.innerHTML.includes('F')) {
                    tempSpan.innerHTML = tempC;
                    unitSpan.innerHTML = C;
                }
                else {
                    console.log("Forgot what Unit we were using, default to F");
                    tempSpan.innerHTML = tempF;
                    unitSpan.innerHTML = F;
                }
            }}, "Convert Unit"),
            // m("button", {onclick: async function (e) {
            //     e.preventDefault();
            //     var w = await fetchWeather(wAPIkey);
            //     console.log(Weather.data);
            //     m.redraw();
            // }}, "Force Update Weather"),
            m("#notice"),
            m("button", {onclick: function (e) {
                e.preventDefault();
                logTemp(tempC, tempF);
            }}, "Save Data to Server"),
            m("button", {onclick: function (e) {
                e.preventDefault();
                fetchLoggedTemps();
                document.getElementById("tempTable").style.display = "block";
            }}, "Load Data from Server"),
            m("button", {onclick: function (e) {
                e.preventDefault();
                localStorage.clear();
                m.route.set("/");
            }}, "Log Out"),
            m("table#tempTable", [
                m("thead",
                    m("tr", [
                        m("th", {scope: "col"}, m.trust("&deg; C")),
                        m("th", {scope: "col"}, m.trust("&deg; F")),
                        m("th", {scope: "col"}, "Timestamp (UTC)")
                    ])
                ),
                m("tbody#temps")
            ])
        ]);
    }
};

function fetchWeather(key, query = "saint%20paul") {
    return m.request({
        method: "GET",
        url: `http://api.weatherstack.com/current?access_key=${key}&query=${query}`,
        responseType: "json"
    }).then(function (response) {
        // console.log(response);
        if (response.success == false) {
            Weather.error = response;
            console.log(response)
            return response;
        }
        Weather.data = response;
        return response;
    }).catch(function (e) { console.log("failed to fetch weather"); console.log(e);});
}

function login(user, pass, attrs) {
    var lForm = new FormData();
    lForm.append("user", user);
    lForm.append("pass", pass);
    localStorage.clear();
    return m.request({
        method: "POST",
        url: cAPIurl+"/login.php",
        body: lForm,
        responseType: "json"
    }).then(function (response) {
        // console.log(response);
        if (response.hasOwnProperty("sucess") && response.success === false) {
            response.errors.forEach(function (i) {
                document.getElementById('notice').innerHTML += i + "<br>";
            });
        }
        else if (response.hasOwnProperty("token")) {
            localStorage.setItem('token', response.token);
            m.route.set("weather");
        }
    }).catch(function (e) {console.log("failed to log in"); console.log(e);});
}

function fetchLoggedTemps(){
    return m.request({
        method: "GET",
        url: cAPIurl+"/weather.php",
        responseType: "json"
    }).then(function (response) {
        // console.log(response);
        // modify table#tempTable tbody
        tbody = document.getElementById("temps");
        tbody.innerHTML = "";
        response.forEach(function(i) {
            tbody.innerHTML += "<tr><td>"+i.temp_c+"</td><td>"+i.temp_f+"</td><td>"+i.timestamp+"</td></tr>";
        });
    }).catch(function (e) {console.log("failed to upload data"); console.log(e);});;
}

function logTemp(tempC, tempF){
    var tForm = new FormData();
    tForm.append("tempC", tempC);
    tForm.append("tempF", tempF);
    return m.request({
        method: "POST",
        url: cAPIurl+"/weather.php",
        body: tForm,
        responseType: "json"
    }).then(function (response) {
        // console.log(response);
        // modify div#notice
    }).catch(function (e) {console.log("failed to upload data"); console.log(e);});;
}

fetchWeather(wAPIkey);

m.route(document.body, "/", {
    "/": Login,
    "/login": Login,
    "/weather": Weather
});
