<?php

class Weather
{
    private $tempC = 0;
    private $tempF = 0;
    private $err_msgs = [];

    private function get_clean_temp_c(){
        $this->tempC = filter_input(INPUT_POST, 'tempC', FILTER_SANITIZE_NUMBER_INT);
        $_POST['tempC'] = 0;
        if ($this->tempC === FALSE || $this->tempC === NULL) {
            $this->err_msgs[] = 'Please supply a tempC.';
            return FALSE;
        }
        return TRUE;
    }

    private function get_clean_temp_f(){
        $this->tempF = filter_input(INPUT_POST, 'tempF', FILTER_SANITIZE_NUMBER_INT);
        $_POST['tempF'] = 0;
        if ($this->tempF === FALSE || $this->tempF === NULL) {
            $this->err_msgs[] = 'Please supply a tempF.';
            return FALSE;
        }
        return TRUE;
    }

    private function init_mysqli(){
        return new mysqli("localhost", 'web', '7At1GzXjoyST', 'code_demo');
    }

    public function update(){
        if ($this->tempC == 0) {
            $cSet = $this->get_clean_temp_c();
        }
        if ($this->tempF == 0) {
            $fSet = $this->get_clean_temp_f();
        }
        if ($cSet == FALSE || $fSet == FALSE) {
            return $this->err_msgs;
        }
        $mysqli = $this->init_mysqli();
        $query = "INSERT INTO weather (temp_c, temp_f) VALUES (".$this->tempC.", ".$this->tempF.");";
        $result = $mysqli->query($query);
        if ($result === TRUE) {
            // $result->free();
            return TRUE;
        }
        else {
            $this->err_msgs[] = 'Creating user failed.';
            // $this->err_msgs['query'] = $query;
            // $this->err_msgs['mysql'] = $mysqli->error_list;
            // $result->free();
            return $this->err_msgs;
        }
    }
    
    public function read(){
        $mysqli = $this->init_mysqli();
        $query = "SELECT temp_c, temp_f, `timestamp` FROM weather LIMIT 100;";
        $result = $mysqli->query($query);
        if ($result === FALSE) {
            $this->err_msgs[] = 'Error reading data';
            // $this->err_msgs['query'] = $query;
            // $this->err_msgs['mysql'] = $mysqli->error_list;
            return $this->err_msgs;
        }
        $weatherArr = [];
        while ($row = $result->fetch_assoc()) {
            $weatherArr[] = $row;
        }
        $result->free();
        return $weatherArr;
    }
}
?>