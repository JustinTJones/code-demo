<?php
require_once('cors.php');
require_once('Weather.php');
$weather = new Weather();

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    echo json_encode($weather->read());
}
else if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $res = $weather->update();
    if ($res === TRUE) {
        echo json_encode(['success'=>TRUE]) . "\n";
    }
    else {
        echo json_encode(['success'=>FALSE, 'errors'=>$res]) . "\n";
    }
}