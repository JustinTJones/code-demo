<?php 
require_once('cors.php');
require_once('User.php');

$user = new User();
$uResponse = $user->login();
$responseArr = [];
if (is_array($uResponse)) {
    $responseArr['success'] = FALSE;
    $responseArr['errors'] = $uResponse;
}
else {
    $responseArr['success'] = TRUE;
    $responseArr['token'] = $uResponse;
}
echo json_encode($responseArr) . "\n";
?>