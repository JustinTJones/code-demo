<?php

class User
{
    private $username = '';
    private $passhash = '';
    private $err_msgs = [];

    private function get_clean_username(){
        $this->username = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
        $_POST['user'] = '';
        if ($this->username === FALSE || $this->username === NULL) {
            $this->err_msgs[] = 'Please supply a username.';
            return FALSE;
        }
        return TRUE;
    }

    private function get_clean_pass(){
        $pass = filter_input(INPUT_POST, 'pass', FILTER_SANITIZE_STRING);
        $_POST['pass'] = '';
        if ($pass === FALSE || $pass === NULL) {
            $this->err_msgs[] = 'Invalid Password';
            return FALSE;
        }
        return $pass;
    }

    private function get_clean_passhash(){
        $this->passhash = password_hash(filter_input(INPUT_POST, 'pass', FILTER_SANITIZE_STRING), PASSWORD_BCRYPT);
        $_POST['pass'] = '';
        if ($this->passhash === FALSE || $this->passhash === NULL) {
            $this->err_msgs[] = 'Invalid Password';
            return FALSE;
        }
        return TRUE;
    }

    private function init_mysqli(){
        return new mysqli("localhost", 'web', '7At1GzXjoyST', 'code_demo');
    }

    public function register_new_user(){
        if (empty($this->username)) {
            $uSet = $this->get_clean_username();
        }
        if (empty($this->passhash)) {
            $pSet = $this->get_clean_passhash();
        }
        if ($uSet == FALSE || $pSet == FALSE) {
            return $this->err_msgs;
        }
        $mysqli = $this->init_mysqli();
        $query = "INSERT INTO users (username, pwhash) VALUES (\"".$this->username."\", \"".$this->passhash."\");";
        $result = $mysqli->query($query);
        if ($result === TRUE) {
            // $result->free();
            return TRUE;
        }
        else {
            $this->err_msgs[] = 'Creating user failed.';
            // $this->err_msgs['query'] = $query;
            // $this->err_msgs['mysql'] = $mysqli->error_list;
            // $result->free();
            return $this->err_msgs;
        }
    }
    
    public function login(){
        if (empty($this->username)) {
            $uSet = $this->get_clean_username();
        }
            $pass = $this->get_clean_pass();
        if ($uSet === FALSE || $pass === FALSE) {
            return $this->err_msgs;
        }
        $mysqli = $this->init_mysqli();
        $query = "SELECT * FROM users WHERE username = \"".$this->username."\" LIMIT 1;";
        $result = $mysqli->query($query);
        if ($result === FALSE) {
            $this->err_msgs[] = 'Invalid username or password.';
            // $this->err_msgs['query'] = $query;
            // $this->err_msgs['mysql'] = $mysqli->error_list;
            return $this->err_msgs;
        }
        $userArr = $result->fetch_assoc();
        if (!password_verify($pass, $userArr['pwhash'])) {
            $this->err_msgs[] = 'Invalid username or password.';
            return $this->err_msgs;
        }
        $result->free();
        return hash('sha256', $userArr['id'] . '|' . $userArr['username']);
    }
}
?>