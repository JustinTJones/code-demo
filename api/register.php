<?php 
require_once('cors.php');
require_once('User.php');

$user = new User();
$uResponse = $user->register_new_user();
$responseArr = [];
if ($uResponse === TRUE) {
    $responseArr['success'] = TRUE;
}
else {
    $responseArr['success'] = FALSE;
    $responseArr['errors'] = $uResponse;
}
echo json_encode($responseArr) . "\n";
?>
